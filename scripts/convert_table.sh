mkdir V3_rar_6000_tsv/
for file in ./V3_rar_6000/*
do
	file_name="$(basename "$file" | cut -d "." -f 1)"
	biom convert -i "$file" -o V3_rar_6000_tsv/"$file_name" --table-type "OTU table" --to-tsv
done
