cd relab
mkdir no_primers
for file in ./*R1*
do 
	awk -f ../scripts/remove_primer_fwd.awk "$file" > no_primers/"$file"
done
for file in ./*R2*
do
	awk -f ../scripts/remove_primer_rvr.awk "$file" > no_primers/"$file"

done