for file in ./flat/*
do 
	sample_number="$(basename "$file" | cut -d "-" -f 1)"
	sample_ID="$(basename "$file")"
	awk -f relab.awk inc="$sample_number" "$file" > relab/"$sample_ID"

done
