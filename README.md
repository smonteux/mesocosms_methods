
**Meshes in mesocosms control solute and biota transfer in soils: a step towards disentangling (a)biotic impacts on the fate of thawing permafrost**

Maria Väisänen, Eveline J. Krab, Sylvain Monteux, Laurenz M. Teuber, Konstantin Gavazov, James T. Weedon, Frida Keuper, Ellen Dorrepaal

_Applied Soil Ecology_  **2020**, [https://doi.org/10.1016/j.apsoil.2020.103537](https://doi.org/10.1016/j.apsoil.2020.103537)

**Foreword**
Sequence data is archived here and at ENA under project accession number PRJEB31437. 
The sequence data deposited in both repositories have undergone the same initial handling:  the outcome of the GenerateFASTQ demultiplexing pipeline provided by Illumina through the BaseSpace web app was stripped of its primers (21 first bp of the forward reads and 18 first bp of the reverse reads, see *scripts/pre-process/remove_primers.sh*). The Nextera dual-indexes part of the fastq sequence header (forward_barcode + reverse_barcode) was further replaced by the index numbers for convenience (see *scripts/pre-process/rename_barcodes.sh*). Thus if we take for instance sample 14 forward reads file, the first read was initially:

    @M03699:70:000000000-B4KHV:1:1101:14651:1121 1:N:0:NTAGTACG+NCTGCATA
    CCTACGGGCGGCAGCAGTAAGGAATATTGGTCAATGGTGGGAACACTGAACCAGCCATTCCGCGTGCAGGATGAATGCCNNNNNNNTTGTAAACNNNNTTAGTCTGGGAAGAAAAAGACCTNNNNNNNGGTAACTGACGGTANNNNNNNAATAAGCNNNNNNNAACTCCGTGCCAGCNGNNNCGGTAATACGGAGGGTGCAAGCGTTATCCGGAATTACTGGGTTTAAAGGGTGNGTAGGCGGTTATATAAGTCAGCAAGTGAAATTCCACAGCTTAACTGTAGGGACTGCTTTTGATACT
    +
    88ACCGGGGGGGGGGGGGGGGGFFGGGGGGGGGGGGGFGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGFGGGGGGG#######::DFGGGG####::FGGGGGGGGGGGGGGGGFGGF#######88DFGGGGGGGGGF#######+8@FGGA#######66@DGGGGGGGFG@#6###3/

  And is now
  

    @M03699:70:000000000-B4KHV:1:1101:14651:1121 1:N:0:14
    GTAAGGAATATTGGTCAATGGTGGGAACACTGAACCAGCCATTCCGCGTGCAGGATGAATGCCNNNNNNNTTGTAAACNNNNTTAGTCTGGGAAGAAAAAGACCTNNNNNNNGGTAACTGACGGTANNNNNNNAATAAGCNNNNNNNAACTCCGTGCCAGCNGNNNCGGTAATACGGAGGGTGCAAGCGTTATCCGGAATTACTGGGTTTAAAGGGTGNGTAGGCGGTTATATAAGTCAGCAAGTGAAATTCCACAGCTTAACTGTAGGGACTGCTTTTGATACT
    +
    GGGGGGFFGGGGGGGGGGGGGFGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGFGGGGGGG#######::DFGGGG####::FGGGGGGGGGGGGGGGGFGGF#######88DFGGGGGGGGGF#######+8@FGGA#######66@DGGGGGGGFG@#6###3/
The sequence data deposited here and at ENA are identical, however please note that ENA handling alters fastq sequence headers which I have not accounted for in this here script, therefore the md5sums will differ and this script will only work with the files on ENA if the submitted version of the files is downloaded. To do so, download the sequence files using enaBrowserTools' enaDataGet or enaGroupGet with the "-f submitted" flag.  

**Repository structure**

-   Vaisanen2020ASE_mesoc_methods_bioinfo.ipynb is the core bioinformatics pipeline file (Jupyter notebook)
    
-   R/ stores the data files and script (Vaisanen2020ASE_bacteria.R) that can be used as they are to reproduce the published results relating  to bacterial communities (Fig 3, Table S3, Fig. S1)
    
-   raw/ stores the raw .fastq files mentioned above
    
-   R/Mapping_m17.txt is the QIIME mapping file used throughout the Jupyter notebook
    
-   scripts/ store various scripts and databases used in the bioinformatics pipeline
    
    -   97_otu_taxonomy.txt and 97_otus.fasta are GreenGenes 13.8 database files (McDonald et al., 2012, ISME J)
        
    -   clean_tsv_from_biom_rarefied_tables.sh batch-runs cleaner.awk on V3_rar_6000_tsv/ into V3_rar_6000_tsv/clean/
        
    -   cleaner.awk cleans up tsv files created from biom files for easier import in R, removing all comment characters "#", the first line ("Constructed from biom file") and modifying the second line ("OTU Id" becomes "OTU_Id")
        
    -   convert_table.sh batch converts the 100 rarefactions from biom to tsv into Bsnowfence_rarefied1000_tsv/
        
    -   failed_pcr_list.txt contains the ID of two samples which did not yield any correct-sized PCR product and are removed from analysis after the mapping of reads to chimera-filtered OTUs
        
    -   gold.fa is the GOLD database (Haas et al., 2011, Genome Research)
        
    -   make_consensus_from_rarefactions.R computes an average abundance table from the 100 tables in V3_rar_6000_tsv/clean/
    - pre-process/ contains the scripts mentioned in the opening, used to remove primers (as required for ENA submission) and adapt fastq header lines for convenience 
        
    -   python_scripts/  are Robert Edgar's UPARSE supporting Python scripts (https://drive5.com/python/python_scripts.tar.gz)
        
    -   relabel.awk is custom-made by J.T.Weedon and changes the Illumina headers of a fastq file into a simpler format (>S*sequence_number*; barcode=*index_number*)
                
-   Rnew/ will be created as the final step of the Jupyter notebook, and will have a similar structure to the R/ folder. The results should match those obtained using the R/ folder save for possible stochastic differences (e.g. in rarefactions)
    

**Contact**

-   Corresponding author: Maria Väisänen  [maria.vaisanen@oulu.fi](mailto:maria.vaisanen@oulu.fi)  [ResearchGate](https://www.researchgate.net/profile/Maria_Vaeisaenen)
    
-   For bugs or questions regarding the repository / bioinformatics: Sylvain Monteux [sylvain.monteux@slu.se](mailto:sylvain.monteux@slu.se)  [ResearchGate](https://www.researchgate.net/profile/Sylvain_Monteux)

